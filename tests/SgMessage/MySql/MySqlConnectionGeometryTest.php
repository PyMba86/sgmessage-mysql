<?php

namespace Test\SgMessage\MySql;

use PDO;
use PHPUnit\Framework\TestCase;
use SgMessage\Geometry\Type\PointGeometry;
use SgMessage\MySql\Strategy\Geometry\MySqlConnectionGeometry;
use SgMessage\SgMessageBuilder;
use SgMessage\Strategy\Geometry\GeometryStrategy;
use SgMessage\Strategy\Geometry\Action\PointDistanceSphereGeometryAction;

class MySqlConnectionGeometryTest extends TestCase
{
    /**
     * @var string
     */
    public $dsn = "mysql:host=127.0.0.1;dbname=dbsok;charset=utf8";

    /**
     * @var string
     */
    public $username = 'root';

    /**
     * @var string
     */
    public $password = 'root';

    /**
     * @var array
     */
    public $options = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_PERSISTENT => false,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8 COLLATE utf8_unicode_ci"
    ];


    public function testConnectMySqlConnection()
    {
        $pdo = new PDO($this->dsn, $this->username, $this->password);

        $point = new PointGeometry(61.015032, 69.056268);

        $connection = new MySqlConnectionGeometry($pdo);
        $geometryStrategy = new GeometryStrategy($connection);
        $geometryStrategy
            ->table("messages")
            ->columns(["id", "name", "location"])
            ->primary("id")
            ->actions([
                new PointDistanceSphereGeometryAction($point, "location", 0.2)
            ]);

        $builder = new SgMessageBuilder();
        $messages = $builder
            ->strategies([
                    $geometryStrategy
                ]
            )->build();

        $this->assertEquals(2, count($messages));
    }
}
