<?php

namespace SgMessage\MySql\Strategy\Geometry;

use PDO;
use SgMessage\Strategy\Geometry\ConnectionGeometryInterface;
use SgMessage\Strategy\Geometry\ConnectionQuoterGeometry;

class MySqlConnectionGeometry implements ConnectionGeometryInterface
{
    /**
     * @var PDO
     */
    public $pdo;

    /**
     * @var ConnectionQuoterGeometry
     */
    public $quoter;

    /**
     * Коннектор mysqli
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
        $this->quoter = new ConnectionQuoterGeometry();
    }

    /**
     * @inheritdoc
     */
    public function query(string $q): array
    {
        return $this->pdo->query($q)->fetchAll();
    }

    /**
     * @inheritdoc
     */
    public function getQuoter(): ConnectionQuoterGeometry
    {
        return $this->quoter;
    }
}
